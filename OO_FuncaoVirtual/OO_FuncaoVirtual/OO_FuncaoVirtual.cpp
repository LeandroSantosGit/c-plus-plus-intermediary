// OO_FuncaoVirtual.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class ClasseBase { //classe

public:
	virtual void Virtual() { //classe definida como virtual podera ser redefinida por outra classe
		cout << "Estou na classe Base" << endl;
	}
};

class Derivada :public ClasseBase { //classe derivada
public:
	void Virtual() { //redefinindo mensagem do metodo virtual
		cout << "Estou na classe Derivada" << endl;
	}
};

int main()
{
	ClasseBase *p; //ponteiro
	ClasseBase base; //ponteiro ira apontar para classe base
	Derivada derivada; //ponteiro ira apontar para classe derivada

	p = &base; //ponteiro esta apontando para classe base
	p->Virtual();

	p = &derivada; //ponteiro esta apontando para classe base
	p->Virtual();

	system("pause");

    return 0;
}

