// OO_Sobrecarga.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include<iostream>

using namespace std;

void Soma(int *x, int *y);
void Soma(float *x, float *y);

int main() {

	float x = 2.5;
	float y = 3.6;

	Soma(&x, &y);

	int x1 = 15;
	int y1 = 7;

	Soma(&x1, &y1);

	system("pause");

	return 0;
}

void Soma(int *x, int *y) {

	int resultado = *x + *y;
	cout << resultado << endl;
}

void Soma(float *x, float *y) {

	float resultado = *x + *y;
	cout << resultado << endl;
}

/* sobrecarga: funcao com mesmo nome e argumentos diferentes*/