// EstruturaDados_ListaEncadeadaDupla.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class No { //classe para criar no
	friend class Lista; //classe Lista sera	membro para acessar atributos e metodos privados

private: //atributos
	int valor; //valor que sera adionado na lista
	No *Pproximo; //ponteiro pra poximo no

public: //metodos
	No(int val) : valor(val), Pproximo(NULL) { //metodo construtor com argumentos 
		 //valor que sera adicionado na lista, Pproximo aponta para o final da lista(NULL)
	}
};

class Lista {

private: //atributos
	No * PInicio; // inicio da lista
	No * PFim; //fim da lista
	No *PegaNo(int v); //alocar novo no

public: //metodos
	Lista(void);

	void AdicionaNoFrente(int val); //adicionar um No na frente
	void AdicionaNoAtras(int val); //adicionar um No na atras
	void RemoveNo(int val); //remove No
	void Imprimir(); //Imprimir
};

//implementar a funcao para alocar novo no
Lista::Lista() :PInicio(NULL), PFim(NULL) {

}

//implementar o metodo para adcionar na frente da lista
void Lista::AdicionaNoFrente(int val) {

	No *AuxiliaP = PegaNo(val); //criamos um ponteiro 

	if(PInicio == NULL) { //se estiver vazia
		PInicio = PFim = AuxiliaP; //criar com valor qualquer 
	}
	else { //se nao estiver vazia
		AuxiliaP->Pproximo = PInicio; //adcionar novo valor na frente da lista
		PInicio = AuxiliaP;	
	}
}

//implementar o metodo para adcionar no fim da lista
void Lista::AdicionaNoAtras(int val) {

	No *AuxiliaP = PegaNo(val); //criamos um ponteiro

	if (PInicio == NULL) { //se estiver vazia
		PInicio = PFim = AuxiliaP; //criar com valor qualquer
	}
	else { //se nao estiver vazia
		PFim->Pproximo = AuxiliaP; //adcionar novo valor no final da lista
		PFim = AuxiliaP;
	}
}

void Lista::RemoveNo(int val) {
	No *pAuxiliar = NULL; //ponteiros irao auxilicar na exclusao
	No *pDeletar = NULL;

	if (PInicio->valor == val) { //	verificar se e o no principal
		pDeletar = PInicio; // excluir no inicio da lista
		PInicio = pDeletar->Pproximo;
		delete pDeletar;
	}

	pAuxiliar = PInicio; 
	pDeletar = PInicio->Pproximo;

	//percorrer a lista verificando cada no para atulizar e atualizar final da lista
	while (pDeletar != NULL) { 
		if (pDeletar->valor == val) {
			pAuxiliar->Pproximo = pDeletar->Pproximo;

			if (pDeletar == PFim) {
				PFim = pAuxiliar;
			}

			delete pDeletar;
			break;
		 }

		pAuxiliar = pDeletar;
		pDeletar = pDeletar->Pproximo;
	}
}

No *Lista::PegaNo(int valor) { //criamos novo no para adicionar no metodos de inserir na frente ou atras
	No *aux = new No(valor);
	return aux;
}

void Lista::Imprimir() { //imprimir lista	

	No *p = PInicio;

	if (PInicio == NULL) {
		cout << "Lista vazia" << endl;
	}

	cout << "Lista: ";
	while (p != NULL) {
		cout << p->valor << " ";
		p = p->Pproximo;
	}
	cout << endl;
}

int main()
{
	Lista lista; //criamos lista

	//adicionamos valores no inicio e no fim
	lista.AdicionaNoFrente(10);
	lista.AdicionaNoFrente(20);
	lista.AdicionaNoAtras(5);
	lista.AdicionaNoAtras(30);
	lista.AdicionaNoFrente(4);

	lista.Imprimir(); //imprimir lista

	lista.RemoveNo(20); //remover valor da lista

	lista.Imprimir();//imprimir lista

	lista.RemoveNo(10); //remover valor da lista

	lista.Imprimir();//imprimir lista

	system("pause");

    return 0;
}

