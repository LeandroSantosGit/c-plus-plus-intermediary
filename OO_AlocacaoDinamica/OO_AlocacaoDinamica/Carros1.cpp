#include "stdafx.h"
#include <iostream>
#include <cassert>
#include "Carros.h"

using namespace std;

#pragma warning(disable : 4996) //para evitar erro de compilacao no uso so strcpy()

int Carro::count = 0; //iniciar atributo estatico

int Carro::getCount() { //implemento do metodo estatico

	return count;
}

Carro::Carro(const char *N) { //Alocando com o construtor

	Nome = new char[strlen(N) + 1]; //atribuimos N ao ponteiro Nome, strlen passara o tamanho do caracter digitado
	assert(Nome != 0); //assegurar que foi alocado na memoria
	strcpy(Nome, N); //strcpy ira copiar da string N em Nome

	++count; //incremento do count

	cout << "Construtor para carro " << Nome << " foi chamado" << endl;
}

Carro::~Carro() { //desalocar da memoria

	cout << "\nDestruidor para carro " << Nome << " foi chamado" << endl;

	delete[] Nome; //delete para recuperar a memoria

	--count; //decremento do count
}

const char *Carro::getNome() const {

	return Nome;
}