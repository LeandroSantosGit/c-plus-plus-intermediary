#pragma once

class Carro {

public: 
	 
	Carro(const char*); //metodo construtor

	~Carro(); //metodo destruidor

	const char *getNome() const; //retorno do nome

	static int getCount(); //metodo estatico para retornar numero de objetos instanciados

private: //atributos
	char *Nome;
	static int count; //numeros de dados instanciados 
};