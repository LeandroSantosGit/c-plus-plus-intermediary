// OO_AlocacaoDinamica.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include "Carros.h"

using namespace std;

int main()
{
	cout << "Numero de carros antes da instancia" << endl;
	cout << Carro::getCount() << endl; //retornar valor de count

	//instanciando carros
	cout << "\n\n";
	Carro *Car01 = new Carro("Gol"); 
	cout << "\n\n";
	Carro *Car02 = new Carro("Corsa");
	cout << "\n\n";

	cout << "Numeros de carros depois da instancia" << endl;
	cout << Car01->getCount() << endl; //retornar quantos carros foram instanciados
	cout << "\n  Carro 1 = " << Car01->getNome() << endl; //exibir nome dos carros
	cout << "  Carro 2 = " << Car02->getNome() << endl;

	//deletando os carros e recuperando memoria
	delete Car01; //deletar a instancia
	Car01 = 0; //recuperando memoria
	delete Car02;
	Car02 = 0;

	cout << "\nNumero de carros apos o delete" << endl;
	cout << Carro::getCount() << endl; //retornar valor de count

	system("pause");

    return 0;
}

