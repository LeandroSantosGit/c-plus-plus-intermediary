#pragma once

namespace BancoDadosPrimeiroPrograma {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;
	using namespace System::Data::Sql;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			InitializeDataGridView();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	protected:
	private: System::Windows::Forms::BindingSource^  bindingSource1;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(49, 22);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(373, 150);
			this->dataGridView1->TabIndex = 0;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(473, 301);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			this->ResumeLayout(false);

		}

	private:
		static MyForm^ m_DefaultInstance;
		static Object^ m_SyncObject = gcnew Object;

		void InitializeDataGridView() {
			try {

				dataGridView1->AutoGenerateColumns = true;

				bindingSource1->DataSource = GetData("Select * From Alunos");
				dataGridView1->DataSource = bindingSource1;

				dataGridView1->AutoSizeRowsMode = DataGridViewAutoSizeRowsMode::DisplayedCellsExceptHeaders;

				dataGridView1->BorderStyle = BorderStyle::Fixed3D;

				dataGridView1->EditMode = DataGridViewEditMode::EditOnEnter;
				
			}
			catch (SqlException^ ex) {
				MessageBox::Show(ex->ToString());
			}
			catch (System::Exception^ excessao) {
				MessageBox::Show(excessao->ToString());
			}
		}

		DataTable^ GetData(String^ sqlCommand) {

			String^ connectionString = "Data Source=COMPUTADORVASCO\SQLSERVER;Initial Catalog=PrimeiroBanco;Integrated Security=True";
			SqlConnection^ Connection = gcnew SqlConnection(connectionString);
			SqlCommand^ command = gcnew SqlCommand(sqlCommand, Connection);
			SqlDataAdapter^ adapter = gcnew SqlDataAdapter;
			adapter->SelectCommand = command;
			DataTable^ table = gcnew DataTable;
			adapter->Fill(table);
			return table;
		}

#pragma endregion
	};
}
