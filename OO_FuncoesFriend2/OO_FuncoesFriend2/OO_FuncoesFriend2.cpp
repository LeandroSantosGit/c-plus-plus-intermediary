// OO_FuncoesFriend2.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Valor {

	friend void setVal(Valor &, int, int); //declarar friend

public:
	Valor() { //metodo construtor
		X = 0;
		Y = 0;
	}

	void imprimir() const { //metodo para imprimir

		int Result = X + Y;

		cout << "X = " << X << endl;
		cout << "Y = " << Y << endl;
		cout << "Resultado = " << Result << endl;
	}

private:
	int X;
	int Y;
};

//Implementar metodo friend
void setVal(Valor &x, int valor1, int valor2) { 

	x.X = valor1; //adicionar valores em x e y
	x.Y = valor2;
}

int main()
{
	Valor val; //criado objeto

	val.imprimir(); //chamar metodo imprimir 

	setVal(val, 10, 20); //passar valores para x e y

	val.imprimir();

	system("pause");

    return 0;
}

