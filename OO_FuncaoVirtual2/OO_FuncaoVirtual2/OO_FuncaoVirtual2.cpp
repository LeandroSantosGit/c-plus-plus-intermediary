// OO_FuncaoVirtual2.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class ClasseBase {
public:
	virtual void Soma() {
		int resultado;
		resultado = 2 + 2;

		cout << "Base " << resultado << endl;
	}
};

class ClasseDerivada:public ClasseBase {

public:
	void Soma() {
		float resultado;
		resultado = 2.2 + 2.3;

		cout << "Derivada " << resultado << endl;
	}
};

int main()
{
	ClasseBase *p; //ponteiro
	ClasseBase base; //calsse que ponteiro ira apontar
	ClasseDerivada derivada; //calsse que ponteiro ira apontar

	p = &base; //apontando para classe base
	p->Soma();

	p = &derivada; //apontando para classe derivada
	p->Soma();

	system("pause");

    return 0;
}

