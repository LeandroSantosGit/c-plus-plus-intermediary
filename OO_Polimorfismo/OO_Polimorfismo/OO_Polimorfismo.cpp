// OO_Polimorfismo.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Base {

public:
	virtual int Soma(int x, int y) {
		return(x + y);
	}
};

class Derivada1 : public Base {

	virtual int Soma(int x, int y) {
		cout << "Classe Derivada1: " << x + y << endl;
		return(x + y);
	}
};

class Derivada2 : public Base {

	virtual int Soma(int x, int y) {
		cout << "Classe Derivada2: " << x + y << endl;
		return(x + y);
	}
};

int main()
{
	Base *d; //aplicamos o polimorfismo, com o objeto ponteiro podera apontar para qualquer classe

	d = new Derivada1;
	d->Soma(10, 2);

	d = new Derivada2;
	d->Soma(5, 2);

	system("pause");
	
    return 0;
}

//Polimorfismo é a capacidade do mesmo objeto assumir formas diferentes
