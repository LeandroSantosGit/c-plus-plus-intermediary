#pragma once

#ifndef PILHA_H
#define PILHA_H

template<class T> //iniciamos gabarito 
class Pilha { //criamos classe

public: 
	//tamanho default da pilha 
	Pilha(int = 5);

	//destruidor
	~Pilha() {
		delete[] pilhaP;
	}

	//insere na pilha 
	bool InsereP(const T&);

private:
	int posicaoTop;
	int tamanho;
	T *pilhaP;

	bool VaziaP() const {
		return posicaoTop == -1;
	}

	bool CheiaP() const {
		return posicaoTop == tamanho - 1;
	}
};

//Construtor default 
template<class T> //gabarito 
Pilha<T>::Pilha(int tam) { //metodo da classe 

	tamanho = tam > 0 ? tam : 5;
	posicaoTop = -1;
	pilhaP = new T[tamanho];
}

//Insere
template<class T> //gabarito 
bool Pilha<T>::InsereP(const T &ValorIn) { //metodo da classe

	if (!CheiaP()) {
		pilhaP[++posicaoTop] = ValorIn;
		return true;
	}

	return false;
}

#endif //PILHA_H


