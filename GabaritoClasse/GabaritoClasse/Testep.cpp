// GabaritoClasse.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include "pilha.h"

using namespace std;

int main()
{
	Pilha<float> floatPilha(5); //instancia objeto igual a 3
	float valor = 1.0; //valor inicial 
	cout << "Insere " << endl;

	while (floatPilha.InsereP(valor)) { //enquanto nao atingir floatPilha inserir valores

		cout << valor << endl; //imprimir valor
		valor += 1.1; //valor + 1.1
	}

	cout << "A pilha esta cheia" << endl;

	system("pause");

    return 0;
}

