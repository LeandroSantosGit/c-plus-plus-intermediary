## C++ Intermediário

No curso foi abordado como implementar estruturadas de dados; criar formulários; acessar base de dados; trabalhar com o DirectX.

* Orientação a objetos
* Gabaritos
* Tratamento de exceções
* Estruturas de dados
* Interface Gráfica - Windows Forms
* Banco de dados - SQL Server
* DirectX - Direct3D e DirectX 10