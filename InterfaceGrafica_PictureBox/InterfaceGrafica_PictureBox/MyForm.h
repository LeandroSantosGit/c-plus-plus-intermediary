#pragma once

namespace InterfaceGraficaPictureBox {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  comboBox1; //add controle ComboBox no formulario
	protected:
	private: System::Windows::Forms::TextBox^  textBox1; //add controle TextBox no formulario
	private: System::Windows::Forms::PictureBox^  pictureBox1; //add controle PictureBox no formulario
	private: System::Windows::Forms::Label^  label1;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// comboBox1
			// 
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(10) {
				L"C++", L"C", L"C#", L"VB", L"PHP", L"CSS", L"HTML",
					L"HTML5", L"JAVA", L"COBOL"
			});
			this->comboBox1->Location = System::Drawing::Point(31, 84);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(223, 21);
			this->comboBox1->TabIndex = 0;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox1_SelectedIndexChanged);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(31, 172);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(223, 20);
			this->textBox1->TabIndex = 1;
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(348, 60);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(281, 220);
			this->pictureBox1->TabIndex = 2;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Click += gcnew System::EventHandler(this, &MyForm::pictureBox1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(43, 36);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(187, 19);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Selecione a Linguagem";
			this->label1->Click += gcnew System::EventHandler(this, &MyForm::label1_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(695, 311);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->comboBox1);
			this->Name = L"MyForm";
			this->Text = L"Linguagem de programa��o";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {

		if (this->textBox1->Text != "") { //se dados estiver selecionado no textBox entao ele foi selecionado no comboBox,  exibir mensagem
			MessageBox::Show(this->textBox1->Text, "item Selecionado", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
		}
		else { //se nao selecionado exibir menssagem
			MessageBox::Show("Voce nao selecionou!", "Item Selecionado", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
		}
	}
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

		textBox1->Text = this->comboBox1->SelectedItem->ToString(); //elemento selecionado no comboBox aparecera no textBox

		//definir image para ser carregado no pictureBox
		if (textBox1->Text == "C++") { //se textBox for igual ao paremetro
			this->pictureBox1->Image = Image::FromFile("cpp.png"); //exibir nao pictureBox image contina no projeto
		}
		else if (textBox1->Text == "C") {
			this->pictureBox1->Image = Image::FromFile("c.png");
		}
		else if (textBox1->Text == "C#") {
			this->pictureBox1->Image = Image::FromFile("cs.png");
		}
		else if (textBox1->Text == "VB") {
			this->pictureBox1->Image = Image::FromFile("vb.png");
		}
		else if (textBox1->Text == "PHP") {
			this->pictureBox1->Image = Image::FromFile("php.png");
		}
		else if (textBox1->Text == "JAVA") {
			this->pictureBox1->Image = Image::FromFile("java.png");
		}
		else if (textBox1->Text == "CSS") {
			this->pictureBox1->Image = Image::FromFile("css.png");
		}
		else if (textBox1->Text == "HTML") {
			this->pictureBox1->Image = Image::FromFile("html.png");
		}
		else if (textBox1->Text == "HTML5") {
			this->pictureBox1->Image = Image::FromFile("html5.png");
		}
		else if (textBox1->Text == "COBOL") {
			this->pictureBox1->Image = Image::FromFile("cobol.png");
		}
	}
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
};
}
