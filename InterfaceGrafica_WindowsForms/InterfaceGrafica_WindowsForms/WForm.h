#pragma once

namespace InterfaceGraficaWindowsForms {

	using namespace System; //classes fundamentais que definem tipos de dados e valores 
	using namespace System::ComponentModel; //classes para comportamento dos compoentes
	using namespace System::Collections; //classes que definem colecoes de objetos
	using namespace System::Windows::Forms; //classes para criar aplicativos baseados no windows
	using namespace System::Data; //classes da arquitetura ADO.NET  
	using namespace System::Drawing; //acesso ao GDI funcionalidades graficas

	/// <summary>
	/// Sum�rio para WForm
	/// </summary>
	public ref class WForm : public System::Windows::Forms::Form
	{
	public:
		WForm(void) //metodo construtor
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected: 
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~WForm() //metodos destruidor
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1; //adcionar controle label no MyForm
	private: System::Windows::Forms::TextBox^  textBox1; //adcionar controle textBox no MyForm
	private: System::Windows::Forms::Button^  button1; //adcionar controle button no MyForm
	protected:

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components; //encapsular componentes

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void) //metodo para iniciar os componentes
		{
			this->label1 = (gcnew System::Windows::Forms::Label()); //instancia label
			this->textBox1 = (gcnew System::Windows::Forms::TextBox()); //instancia TextBox
			this->button1 = (gcnew System::Windows::Forms::Button()); //instacia button
			this->SuspendLayout();
			// 
			// label1
			// propriedades label
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Verdana", 20, static_cast<System::Drawing::FontStyle>(((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)
				| System::Drawing::FontStyle::Underline)), System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::SystemColors::Highlight;
			this->label1->Location = System::Drawing::Point(224, 128);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(107, 32);
			this->label1->TabIndex = 0;
			this->label1->Text = L"label1";
			this->label1->Click += gcnew System::EventHandler(this, &WForm::label1_Click); //evento registra click
			// 
			// textBox1
			// propriedades TextBox
			this->textBox1->Location = System::Drawing::Point(211, 37);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(134, 20);
			this->textBox1->TabIndex = 1;
			this->textBox1->Click += gcnew System::EventHandler(this, &WForm::textBox1_Click); //evento registra um click
			this->textBox1->DoubleClick += gcnew System::EventHandler(this, &WForm::textBox1_DoubleClick); //evento registra dois clicks
			// 
			// button1
			// propriedades button
			this->button1->Location = System::Drawing::Point(243, 231);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(88, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &WForm::button1_Click); //evento registra click
			// 
			// WForm
			// formulario
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(594, 326);
			this->Controls->Add(this->button1); //adicionar no formulario controle button para execucao 
			this->Controls->Add(this->textBox1); //adicionar no formulario controle TextBox para execucao
			this->Controls->Add(this->label1); //adicionar no formulario controle label para execucao
			this->Name = L"WForm";
			this->Text = L"WForm";
			this->Load += gcnew System::EventHandler(this, &WForm::WForm_Load_1);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion 
	private: System::Void WForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) { //metodo evento click

		this->label1->Text = "TreinaWeb"; //ao clicar ira trocar Text para TreinaWeb
	}
	private: System::Void textBox1_Click(System::Object^  sender, System::EventArgs^  e) { //metodo evento click

		this->textBox1->Text = "Cursos"; //com um click ira aparecer curso na caixa de texto
	}
	private: System::Void textBox1_DoubleClick(System::Object^  sender, System::EventArgs^  e) { //metodo evento duplo click

		this->textBox1->Enabled = false; //com dois cliks desabilita a caixa de texto
	}
	private: System::Void WForm_Load_1(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {	//metodo evento click
	 //ao clicar no button ira exibir uma caixa texto por nome Botao com a msg Evento click adicionado tem um botao ok com sinal de exclamacao
	MessageBox::Show("Evento click adicionado", "Botao", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
}
};
}
