using namespace System;                  //contem classes fundamentais da biblioteca
using namespace System::Windows::Forms;  //comtem classes referente a formulario

#include "WForm.h" 

[STAThread] //anotacao para interoperabilidade do sistema
void main() {

	Application::EnableVisualStyles(); //controle estilo visual do sistema operacional
	Application::SetCompatibleTextRenderingDefault(false); //controle para processar o texto

	InterfaceGraficaWindowsForms::WForm form; //nome do projeto e formulario e criamos variavel do formulario
	Application::Run(%form); //executar aplicacao
}