#pragma once

namespace InterfaceGraficaComboBox {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ComboBox^  comboBox1; //controle ComboBox
	private: System::Windows::Forms::TextBox^  textBox1; //controle TextBox
	protected:

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox()); //instancia ComboBox
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());  //instancia TextBox
			this->SuspendLayout();
			// 
			// comboBox1
			// propriedades ComboBox
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(10) { //lista de nomes que exbira no comboBox
				L"C++", L"C", L"C#", L"VB", L"PHP", L"CSS", L"HTML",
					L"HTML5", L"JAVA", L"COBOL"
			});
			this->comboBox1->Location = System::Drawing::Point(44, 55);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(171, 21);
			this->comboBox1->TabIndex = 0;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::comboBox1_SelectedIndexChanged); //evento gerar click
			// 
			// textBox1
			// propriedades textBox
			this->textBox1->Location = System::Drawing::Point(296, 56);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(164, 20);
			this->textBox1->TabIndex = 1;
			// 
			// MyForm
			// formulario
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(537, 261);
			this->Controls->Add(this->textBox1); //adicionar controle TextBox ao formulario
			this->Controls->Add(this->comboBox1); //adcionar controle ComboBox ao formulario
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void comboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) { //metodo para executar evento

		textBox1->Text = this->comboBox1->SelectedItem->ToString();	//ao selecionar elemento no comboBox ele sera exibido no textBox
	}
	};
}
