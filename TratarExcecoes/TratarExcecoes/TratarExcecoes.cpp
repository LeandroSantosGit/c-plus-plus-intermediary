// TratarExcecoes.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class DivisaoZ {

public: 
	DivisaoZ(int n); //metodo construtor 
	void Msg(); //metodo para exibir mensagem

private:
	int linha;
};

//implementar metodos
DivisaoZ::DivisaoZ(int n) {

	linha = n;
}

void DivisaoZ::Msg() {

	cout << "Erro na linha: " << linha << endl;
}

int main()
{
	int x = 10;
	int y = 0;

	try //disparar excecoes 
	{
		if (y == 0) { //se y for igual a zero 
			throw (DivisaoZ(__LINE__)); //se for zero gerarmos excecao passando classe DivisaoZ
		}
		cout << "A divisao é = " << (float)x / y << endl; 
	}
	catch (DivisaoZ Tratar) //capturar erro 
	{
		Tratar.Msg(); //chamar funcao para exibir mensagem de erro
	}

	system("pause");
	
    return 0;
}

