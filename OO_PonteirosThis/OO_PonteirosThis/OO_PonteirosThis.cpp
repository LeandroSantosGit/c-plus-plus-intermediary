// OO_PonteirosThis.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class PonteirosEx { //criamos classe 

public: //metodos
	PonteirosEx(int);
	void imprimir() const;

private:
	int valor; //atributo
};

PonteirosEx::PonteirosEx(int v) { //implementando o metodo 

	valor = v;
}

void PonteirosEx::imprimir() const { //implementando o metodo

	//formas de acessar o valor
	cout << "Valor = " << valor << endl; //acessando valor de forma direta
	cout << "Valor = " << this->valor << endl; //acessando com this->
	cout << "Valor = " << (*this).valor << endl; //acessando com (*this)
}

int main()
{
	PonteirosEx p(50);
	p.imprimir();

	system("pause");

    return 0;
}

