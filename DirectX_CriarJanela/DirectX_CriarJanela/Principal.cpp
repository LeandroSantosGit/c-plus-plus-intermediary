#include <Windows.h>
#include <tchar.h>

//variaveis para criar janela
LPCTSTR className = TEXT("TreinaWeb-DX10");//classe da janela
HWND hwndHandle = NULL; //variavel da janela
int width = 800; //largura 
int height = 600; //altura

bool InitWin(HINSTANCE hInstance, int width, int height); //metodo para iniciar janela

LRESULT CALLBACK CBack(HWND, UINT, WPARAM, LPARAM); //manipular mesg e fluxograma do windows

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow) { //funcao main

	if (!InitWin(hInstance, width, height)) { //se janela nao iniciar da forma que se deve
		return false; //retornar falso
	} 

	MSG msg = { 0 };
	while (WM_QUIT != msg.message) { //verificar msg do sistema, traduzir, despachar e retornar msg
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) == TRUE) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

bool InitWin(HINSTANCE hInstance, int width, int height) {

	WNDCLASSEX wclas; //definer como a janela ira aparecer na tela

	wclas.cbSize = sizeof(WNDCLASSEX); //tamanho da estutura
	wclas.style = CS_HREDRAW | CS_VREDRAW; //estilo
	wclas.lpfnWndProc = (WNDPROC)CBack; //procedimento CallBack
	wclas.cbClsExtra = 0; //bytes extras para alocar essa classe
	wclas.cbWndExtra = 0; //bytes extras para alocar essa instancia
	wclas.hInstance = hInstance; //lidar com a instancia do aplicativo
	wclas.hIcon = 0; //	icone do aplicatrivo
	wclas.hCursor = LoadCursor(NULL, IDC_ARROW); //cursor padrao
	wclas.hbrBackground = (HBRUSH)(COLOR_WINDOW); //cor de fundo
	wclas.lpszMenuName = NULL; //recurso para menu
	wclas.lpszClassName = className; //nome da classe
	wclas.hIconSm = 0; //icone da barra de tarefas
	RegisterClassEx(&wclas); 

	//criar janela
	hwndHandle = CreateWindow(className, //nome da classe
				TEXT("TreinaWeb-DX10"),  //nome da janela
				WS_OVERLAPPEDWINDOW, //estilo da janela
				CW_USEDEFAULT, //canto superior da janela
				CW_USEDEFAULT, //canto esquerdo da janela
				width,  //largura
				height,	//altura
				NULL,	//indentificador da janela pai
				NULL, //identificador para menu
				hInstance, //instancia do programa
				NULL); //MDI Client

	if (!hwndHandle) { //verifica se janela foi criada
		return false;
	}

	ShowWindow(hwndHandle, SW_SHOW);//apresentar janela na tela
	UpdateWindow(hwndHandle);
	return true;
}

LRESULT CALLBACK CBack(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) { //verificar msg na fila
	case WM_KEYDOWN: //caso aperte esc ira parar programa
		switch (wParam) {
		case VK_ESCAPE: //verificar se realmente foi apertado esc
			PostQuitMessage(0);
			break;
		}
		break;
	case WM_DESTROY: //fechar janela
		PostQuitMessage(0);
		break;
	}
	//retorno da msg para procedimento da janela
	return DefWindowProc(hWnd, message, wParam, lParam);
}