using namespace System::Data::SqlClient; //classe do bando de dados 
using namespace System::Data;

#pragma once

namespace BancoDadosAcessando {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Data::SqlClient;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			InitializeDataGridView(); //iniciar componente DataGridViw
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::TextBox^  textBox2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::BindingSource^  bindingSource1;
	private: System::ComponentModel::IContainer^  components;
	protected:

	private: int idTipo;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->bindingSource1 = (gcnew System::Windows::Forms::BindingSource(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(40, 27);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->Size = System::Drawing::Size(502, 144);
			this->dataGridView1->TabIndex = 0;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm::dataGridView1_CellContentClick);
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button1->Location = System::Drawing::Point(40, 292);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(94, 39);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Cadastro";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button2->Location = System::Drawing::Point(176, 292);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(94, 39);
			this->button2->TabIndex = 2;
			this->button2->Text = L"Pesquisar";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button3->Location = System::Drawing::Point(309, 292);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(94, 39);
			this->button3->TabIndex = 3;
			this->button3->Text = L"Atualizar";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button4
			// 
			this->button4->Font = (gcnew System::Drawing::Font(L"Arial", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->button4->Location = System::Drawing::Point(435, 292);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(94, 39);
			this->button4->TabIndex = 4;
			this->button4->Text = L"Excluir";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(212, 213);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(216, 20);
			this->textBox1->TabIndex = 5;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(212, 244);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(216, 20);
			this->textBox2->TabIndex = 6;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(127, 213);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(45, 16);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Nome";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(127, 245);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(79, 16);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Sobrenome";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 356);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"MyForm";
			this->Text = L"Lista de Alunos";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bindingSource1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}

	DataTable^ GetData(String^ sqlCommand) { //metodo para conectar e selecionar dados na tabela

		String^ connectionString = "Data Source=COMPUTADORVASCO\SQLSERVER;Initial Catalog=PrimeiroBanco;Integrated Security=True"; 
		SqlConnection^ Connection = gcnew SqlConnection(connectionString); //conectar ao banco
		SqlCommand^ command = gcnew SqlCommand(sqlCommand, Connection); //
		SqlDataAdapter^ adapter = gcnew SqlDataAdapter; 
		adapter->SelectCommand = command; // pegar os dados da tabela
		DataTable^ table = gcnew DataTable; //carregar os dados na DataTable
		adapter->Fill(table);
		return table;
	}

	private:void InitializeDataGridView() {

		try {
			dataGridView1->AutoGenerateColumns = true;

			bindingSource1->DataSource = GetData("Select * From Alunos"); //selecionar todos os dados da tabela
			dataGridView1->DataSource = bindingSource1; //passamos os dados para dataGridView1
			dataGridView1->AutoSizeRowsMode = DataGridViewAutoSizeRowsMode::DisplayedCellsExceptHeaders;

			dataGridView1->BorderStyle = BorderStyle::Fixed3D; //add bordas no dataGridView1
		}
		catch (SqlException^) { //tratar caso nao consiga se conectar ao banco
			MessageBox::Show("Problema detectado", "ERROR", MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
			System::Threading::Thread::CurrentThread->Abort();
		}
		catch (System::Exception^ excessao) { //tratar caso nao consiga capturar dados no banco
			MessageBox::Show(excessao->ToString());
		}
	}

	Void CarregaData(int id) {
		String^ connectionString = "Data Source=COMPUTADORVASCO\SQLSERVER;Initial Catalog=PrimeiroBanco;Integrated Security=True";
		SqlConnection^ Connection = gcnew SqlConnection(connectionString);
		Connection->Open(); //abrir conexao
		String^ query = "SELECT nome, sobrenome FROM Alunos WHERE id = '" + id + "'"; //selecionar valor pelo id
		SqlCommand^ command = gcnew SqlCommand(query, Connection);
		SqlDataReader^ reader; //ler dados do banco
		reader = command->ExecuteReader(CommandBehavior::CloseConnection); 

		if (reader->Read()) { //se feita leitura no banco passar para as caixas de texto
			this->textBox1->Text = reader["nome"]->ToString();
			this->textBox2->Text = reader["sobrenome"]->ToString();
		}
	}

	private: System::Void Limpar() { //metodo limpar caixas de texto

		this->textBox1->Text = "";
		this->textBox2->Text = "";
	}

#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

		String^ valor1 = this->textBox1->Text; //pegar valores das textBox
		String^ valor2 = this->textBox2->Text;

		bindingSource1->DataSource = GetData("INSERT INTO Alunos(nome, sobrenome) VALUES('" + valor1 + "','" + valor2 + "')"); //inserir dados no banco
		InitializeDataGridView(); //iniciar para atulizar lista
		Limpar(); 
	}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

	String^ valor1 = this->textBox1->Text; //nome para pesquisa

	bindingSource1->DataSource = GetData("SELECT nome, sobrenome FROM Alunos WHERE nome = '" + valor1 + "'"); //selecionar no banco
	Limpar();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

	String^ valor1 = this->textBox1->Text; //passar para string valores alterados
	String^ valor2 = this->textBox2->Text;

	bindingSource1->DataSource = GetData("UPDATE Alunos SET nome ='" + valor1 + "', sobrenome ='" + valor2 + "' WHERE id = '" + idTipo + "'"); //atualizar no banco
	InitializeDataGridView();
	Limpar();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {

	String^ valor1 = this->textBox1->Text;
	String^ valor2 = this->textBox2->Text;

	bindingSource1->DataSource = GetData("DELETE FROM Alunos WHERE id = '" + idTipo + "'"); //deletar no banco
	InitializeDataGridView();
	Limpar();
}
private: System::Void dataGridView1_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {

	idTipo = Convert::ToInt32(dataGridView1->Rows[e->RowIndex]->Cells[0]->Value); //selecionar valor dataGridView1
	CarregaData(idTipo); //carregar valor nas caixas de texto
}
};
}
