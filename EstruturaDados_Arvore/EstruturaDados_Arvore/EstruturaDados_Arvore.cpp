// EstruturaDados_Arvore.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include <cstddef>

using namespace std;

class NoArvore {

	friend class Arvore; //classe friend

public:
	NoArvore(int valor) :EsquerdaP(0), DireitaP(0), Valor(valor) { //construtor da classe

	}

private:
	NoArvore *EsquerdaP;
	NoArvore *DireitaP;
	int Valor;
};

class Arvore {
	
public: //metodos
	Arvore(); // construtor
	void InsereNo(int valor); //inserir no
	void EmOrdem(); //exibir nos em ordem

private: //atributos
	NoArvore *SuperP; //raiz da arvore
	void InsereNoH(NoArvore **, int);
	void EmOrdemH(NoArvore *);
};

Arvore::Arvore() { //criar arvore vazia
	SuperP = 0;
}

void Arvore::InsereNo(int valor) { //metodo de apoio InsereNoH
	InsereNoH(&SuperP, valor); //chamamos InsereNoH
}

void Arvore::InsereNoH(NoArvore **p, int valor) {

	if (*p == 0) { //verificar se arvore esta vazia
		*p = new NoArvore(valor); // se estiver vazia criamos um no raiz sendo o valor principal
	}
	else { 
		if (valor < (*p)->Valor) { //inserir elemento menor que o valor principal a esquerda
			InsereNoH(&((*p)->EsquerdaP), valor);
		}else{
			if (valor >= (*p)->Valor) { //inserir elemento maior que o valor principal a direita
				InsereNoH(&((*p)->DireitaP), valor);
			}
		}
	}
}

void Arvore::EmOrdem() { //apoio ao exibir EmOrdemH

	EmOrdemH(SuperP);
}

void Arvore::EmOrdemH(NoArvore *p) { //processar em ordem primeiro da esquerda, da raiz e da direita

	if(p != 0) { //verificar se tem no na arvore
		EmOrdemH(p->EsquerdaP);
		cout << p->Valor << " : ";
		EmOrdemH(p->DireitaP);
	}
}

int main()
{
	Arvore arv; //criar arvore

	//inserir nos na arvore
	arv.InsereNo(30);
	arv.InsereNo(2);
	arv.InsereNo(20);
	arv.InsereNo(10);
	arv.InsereNo(1);
	arv.InsereNo(50);

	arv.EmOrdem(); //exibir nos da arvore
	cout << "\n";

	system("pause");

    return 0;
}

