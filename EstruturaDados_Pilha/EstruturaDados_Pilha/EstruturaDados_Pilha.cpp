// EstruturaDados_Pilha.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include <cstddef>

using namespace std;

class No {

	friend class Lista;

private:
	int valor;
	No *Pproximo;

public:
	No(int val) :valor(val), Pproximo(NULL) {

	}
};

class Lista {

private:
	No *PInicio;
	No *PFim;
	No *PegaNo(int v);

public:
	Lista(void);

	void AdicionaNoFim(int val);
	bool RemoveNoFim();
	void Imprimir();
};

Lista::Lista() :PInicio(NULL), PFim(NULL) {

}

void Lista::AdicionaNoFim(int val) {

	No *Auxi = PegaNo(val);

	if (PInicio == NULL) {
		PInicio = PFim = Auxi;
	}
	else {
		Auxi->Pproximo = PInicio;
		PInicio = Auxi;
	}
}

bool Lista::RemoveNoFim() {

	if (PInicio == NULL) {
		return false;
	}
	else {
		No *Ptemp = PInicio;

		if (PInicio == PFim) {
			PInicio = PFim = NULL;
		}
		else {
			PInicio = PInicio->Pproximo;
		}

		delete Ptemp;
		return true;
	}
}

No *Lista::PegaNo(int valor) {

	No *aux = new No(valor);
	return aux;
}

void Lista::Imprimir() {

	No *p = PInicio;

	if (PInicio == NULL) {
		cout << "Pilha vazia" << endl;
	}

	cout << "Fila: ";
	while (p != NULL) {
		cout << p->valor;
		p = p->Pproximo;
	}
	cout << endl;
}

class Pilha :private Lista {

public:
	void entra(int val) {
		AdicionaNoFim(val);
	}

	bool sai() {
		return RemoveNoFim();
	}

	void imprimiF() {
		Imprimir();
	}
};

int main()
{
	Pilha pilha;

	pilha.entra(1);
	pilha.imprimiF();
	pilha.entra(2);
	pilha.imprimiF();
	pilha.entra(3);
	pilha.imprimiF();
	pilha.entra(4);
	pilha.imprimiF();
	pilha.sai();
	pilha.entra(5);
	pilha.imprimiF();
	pilha.entra(6);
	pilha.imprimiF();
	pilha.sai();
	pilha.imprimiF();


	system("pause");

    return 0;
}

