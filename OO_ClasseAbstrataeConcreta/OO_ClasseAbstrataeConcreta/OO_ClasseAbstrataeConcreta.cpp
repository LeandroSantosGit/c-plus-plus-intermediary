// OO_ClasseAbstrataeConcreta.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

class Corredores {

public:
	//metodo construtor
	virtual void velocidade() = 0; //funcao virtual 

	//metodo destruidor
	virtual ~Corredores() {

	}
};

//classes derivadas
class Homen : public Corredores { //classe filha Homen herda classe pai Corredores

public:
	void velocidade() {
		cout << "Rapido " << endl;
	}
};

class Cao : public Corredores { //classe filha Cao herda classe pai Corredores

public:
	void velocidade() {
		cout << "Muito Rapido" << endl;
	}
};

class Cavalo : public Corredores { //classe filha Cavalo herda classe pai Corredores

public:
	void velocidade() {
		cout << "Extremamente Rapido" << endl;
	}
};

int main(void)
{
	//criando objetos
	Corredores *a = new Homen(); 
	Corredores *b = new Cao();
	Corredores *c = new Cavalo();

	//acessar o metodo de cada classe
	a->velocidade();
	b->velocidade();
	c->velocidade();

	system("pause");

    return 0;
}

