// OO_FuncoesFriend.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"

#include <iostream>

using namespace std;

class Valor { //criada classe
	
	//Friend declarada
	friend void setValoX(Valor &, int);

public: //metodos publicos
	Valor() { //metodo com atributo privado 
		X = 0;
	}

	void imprimir() const {
		cout << "X = " << X << endl;
	}

private : //atributo privado
	int X;
};
 
//Implementar a funcao friend
void setValoX(Valor &v, int valor) { //argumento v é de valor
	v.X = valor;
}


int main()
{
	Valor val; //criado objeto
	val.imprimir(); //chamar funcao para imprimir valor

	setValoX(val, 10); //setar o valor 

	val.imprimir(); //chamar funcao para imprimir novo valor setado pela friend

	system("pause");

    return 0;
}

