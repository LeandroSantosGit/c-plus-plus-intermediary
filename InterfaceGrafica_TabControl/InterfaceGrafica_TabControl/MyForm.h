#pragma once

namespace InterfaceGraficaTabControl {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Sum�rio para MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Adicione o c�digo do construtor aqui
			//
		}

	protected:
		/// <summary>
		/// Limpar os recursos que est�o sendo usados.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TabControl^  tabControl1; //add controles
	protected:
	private: System::Windows::Forms::TabPage^  tabPage1;
	private: System::Windows::Forms::RichTextBox^  richTextBox1;
	private: System::Windows::Forms::TabPage^  tabPage2;
	private: System::Windows::Forms::RichTextBox^  richTextBox2;

	private:
		/// <summary>
		/// Vari�vel de designer necess�ria.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necess�rio para suporte ao Designer - n�o modifique 
		/// o conte�do deste m�todo com o editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabPage1 = (gcnew System::Windows::Forms::TabPage());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->tabPage2 = (gcnew System::Windows::Forms::TabPage());
			this->richTextBox2 = (gcnew System::Windows::Forms::RichTextBox());
			this->tabControl1->SuspendLayout();
			this->tabPage1->SuspendLayout();
			this->tabPage2->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabPage1);
			this->tabControl1->Controls->Add(this->tabPage2);
			this->tabControl1->Location = System::Drawing::Point(0, 0);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(489, 303);
			this->tabControl1->TabIndex = 0;
			// 
			// tabPage1
			// 
			this->tabPage1->BackColor = System::Drawing::Color::LightSkyBlue;
			this->tabPage1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->tabPage1->Controls->Add(this->richTextBox1);
			this->tabPage1->Font = (gcnew System::Drawing::Font(L"Arial Narrow", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->tabPage1->Location = System::Drawing::Point(4, 22);
			this->tabPage1->Name = L"tabPage1";
			this->tabPage1->Padding = System::Windows::Forms::Padding(3);
			this->tabPage1->Size = System::Drawing::Size(481, 277);
			this->tabPage1->TabIndex = 0;
			this->tabPage1->Text = L"C++";
			this->tabPage1->Click += gcnew System::EventHandler(this, &MyForm::tabPage1_Click);
			// 
			// richTextBox1
			// 
			this->richTextBox1->Location = System::Drawing::Point(7, 6);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(450, 249);
			this->richTextBox1->TabIndex = 0;
			this->richTextBox1->Text = L"Primeira Aba";
			// 
			// tabPage2
			// 
			this->tabPage2->BackColor = System::Drawing::Color::LightGray;
			this->tabPage2->Controls->Add(this->richTextBox2);
			this->tabPage2->Font = (gcnew System::Drawing::Font(L"Verdana", 8.25F, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->tabPage2->Location = System::Drawing::Point(4, 22);
			this->tabPage2->Name = L"tabPage2";
			this->tabPage2->Padding = System::Windows::Forms::Padding(3);
			this->tabPage2->Size = System::Drawing::Size(481, 277);
			this->tabPage2->TabIndex = 1;
			this->tabPage2->Text = L"Lua";
			this->tabPage2->DoubleClick += gcnew System::EventHandler(this, &MyForm::tabPage2_DoubleClick);
			// 
			// richTextBox2
			// 
			this->richTextBox2->Location = System::Drawing::Point(8, 6);
			this->richTextBox2->Name = L"richTextBox2";
			this->richTextBox2->Size = System::Drawing::Size(445, 251);
			this->richTextBox2->TabIndex = 0;
			this->richTextBox2->Text = L"Segunda Aba";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(501, 315);
			this->Controls->Add(this->tabControl1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->tabControl1->ResumeLayout(false);
			this->tabPage1->ResumeLayout(false);
			this->tabPage2->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void richTextBox2_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	}
private: System::Void tabPage1_Click(System::Object^  sender, System::EventArgs^  e) {
	//ao clicar uma vez na richTextBox1 sera exibido esse texto
	this->richTextBox1->Text = "A Linguagem C foi a base para a cria��o da Linguagem C++, pois a mesma apesar de boa, continha problemas que surgiram de acordo com o aumento do numero de linhas de codigo, sendo assim o C++ veio para solucionar esses e outros problemas menores."
		+ "O criador da Linguagem C++ � o senhor Bjarne Stroustrup.";
}
private: System::Void tabPage2_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
	//ao clicar duas vez na richTextBox sera exibido esse texto
	this->richTextBox2->Text = "Meta - Tabelas nada mais � que um recurso que proporciona ao desenvolvedor modificar o comportamento de uma determinada tabela."
		+ "Quando trabalhamos com meta - tabelas podemos permitir que a linguagem seja capaz de usar a metodologia Orientada a Objetos, assunto que veremos mais adiante e tambem podemos efetuar algumas altera��es que apesar de aparecerem semples tornariam um erro de execu��o caso as meta - tabelas nao fossem usadas.";
}
};
}
