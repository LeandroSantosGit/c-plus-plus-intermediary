#pragma comment(lib,"d3d10.lib") //link da biblioteca
#include <windows.h>
#include <tchar.h>
#include <D3DX10.h>


LPCTSTR className = TEXT("TreinaWeb-DX10");
HWND hwndHandle = NULL;

int width = 800;
int height = 600;

bool InitWin(HINSTANCE hInstance, int width, int height);

ID3D10Device* d3dDevice; //ponteiro que controla hardware
IDXGISwapChain* SwapChain; //ponteiro font e bach buffer
ID3D10RenderTargetView* RenderTargetView; //interface recebe recursos de visualizacao

//manipular background da janela
float red = 0.0f;
int colorChange = 1;

bool InitializeDirect3dApp(HINSTANCE hInstance); //iniciar 3D
bool InitScene(); //cena
void DrawScene(); //desenho da cena

LRESULT CALLBACK CBack(HWND, UINT, WPARAM, LPARAM);

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPTSTR lpCmdLine, int nCmdShow)
{
	if (!InitWin(hInstance, width, height))
	{
		return false;
	}

	if (!InitializeDirect3dApp(hInstance)) //verificar se 3d foi iniciado 
	{
		return false;
	}

	if (!InitScene()) //verificar se cena foi iniciada
	{
		return false;
	}

	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		red += colorChange * 0.00008f; //definimos a cor

		if (red >= 1.0f || red <= 0.0f) //alterar a cor background da janela
		{
			colorChange *= -1;
		}

		D3DXCOLOR bgColor(red, 1.0, 1.0, 1.0f);

		d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor); 

		SwapChain->Present(0, 0); //limpar back buffer

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) == TRUE)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
	return (int)msg.wParam;
}

bool InitWin(HINSTANCE hInstance, int width, int height)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)CBack;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = className;
	wcex.hIconSm = 0;
	RegisterClassEx(&wcex);

	hwndHandle = CreateWindow(
		className,
		TEXT("TreinaWeb-DX10"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		width,
		height,
		NULL,
		NULL,
		hInstance,
		NULL);
	if (!hwndHandle)
	{
		return false;
	}

	ShowWindow(hwndHandle, SW_SHOW);
	UpdateWindow(hwndHandle);
	return true;
}

LRESULT CALLBACK CBack(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			PostQuitMessage(0);
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

bool InitializeDirect3dApp(HINSTANCE hInstance)
{
	DXGI_SWAP_CHAIN_DESC d3d;
	d3d.BufferDesc.Width = width; //largura do buffer swap
	d3d.BufferDesc.Height = height; //altura do buffer swap
	d3d.BufferDesc.RefreshRate.Numerator = 60;
	d3d.BufferDesc.RefreshRate.Denominator = 1;
	d3d.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	d3d.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	d3d.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	d3d.SampleDesc.Count = 1;
	d3d.SampleDesc.Quality = 0;

	d3d.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	d3d.BufferCount = 1;
	d3d.OutputWindow = hwndHandle;
	d3d.Windowed = true;
	d3d.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	d3d.Flags = 0;

	//ajuste para hardware ser mais rapido
	D3D10CreateDeviceAndSwapChain(0, D3D10_DRIVER_TYPE_HARDWARE, 0, 0, D3D10_SDK_VERSION, &d3d, &SwapChain, &d3dDevice);

	//texttura 2D para renderizar back buffer
	ID3D10Texture2D* backBuffer;
	SwapChain->GetBuffer(0, _uuidof(ID3D10Texture2D), reinterpret_cast<void**>(&backBuffer));
	d3dDevice->CreateRenderTargetView(backBuffer, 0, &RenderTargetView);
	backBuffer->Release();

	d3dDevice->OMSetRenderTargets(1, &RenderTargetView, NULL);

	//ajustar posicao, altura, largura, maximo e minimo
	D3D10_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	d3dDevice->RSSetViewports(1, &vp);

	D3DXCOLOR bgColor(0.0f, 0.0f, 0.0f, 1.0f);
	d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor); //limpar tela de renderizacao

	SwapChain->Present(0, 0); //ixibir janela 

	return true;
}

bool InitScene() //preparar pra desenhar cena
{
	return true;
}

void DrawScene() //chamar desenhar cena
{

}