// OO_GabaritoFuncao.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

template<class T> T Soma(T x,T y) { //criamos gabarito que sera reponsavel pela soma

	return (x + y);
}

int main()
{
	//passando valores
	int a = 10, b = 5;
	long c = 1010101L, d = 2020202L;
	float e = 5.2, f = 6.4;

	cout << "Int = " << Soma(a, b) << endl; //imprimir chamando a funcao gabarito com os argumetos 
	cout << "Long = " << Soma(c, d) << endl;
	cout << "Float = " << Soma(e, f) << endl;

	system("pause");

    return 0;
}

