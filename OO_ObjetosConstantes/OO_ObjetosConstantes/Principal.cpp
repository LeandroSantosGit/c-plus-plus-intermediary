// OO_ObjetosConstantes.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"

#include <iostream>
#include "Valores.h" //incluimos a classe Tempo

using namespace std;

//Iniciar dados
Tempo::Tempo(int hr, int min) {//metodo construtor, caso nao seja passado valores ele sera padrao
	setTempo(hr, min); //chamar valor para o metodo
}

void Tempo::setTempo(int h, int m) { //metodo para definir hora no sistema
	setHora(h);
	setMinuto(m);
}

//Iniciar o valor da hora
void Tempo::setHora(int h) { //metodo para implementar hora 
	hora = (h >= 0 && h < 24) ? h : 0; //hora maior ou igual a zero e menor que 24
}

void Tempo::setMinuto(int m) { //metodo para implementar minutos
	minuto = (m >= 0 && m < 60) ? m : 0; //minuto maior ou iguala zero e menor que 60
}

//Metodos constante Pegar valor de hora e munito
int Tempo::getHora() const { //retornar hora 
	return hora;
}

int Tempo::getMinuto() const { //retornar minuto
	return minuto;
}

//Imprimir hora
void Tempo::impressao() const {
	
	// se valor passados for menor que 10, adicionamos um zero a frente (5:5 para 05:05)
	cout << (hora < 10 ? "0" : "") << hora << ":" << (minuto < 10 ? "0" : "") << minuto;
}


int main()
{
	Tempo exemplo2(10, 20); //chamar metodo construtor
	const Tempo exemplo(12, 10); //chamar metodo construtor sendo constante

	exemplo.getHora(); //pegar hora const

	exemplo2.setHora(2);
	//exemplo2.getHora();


	exemplo.impressao();

	cout << "\n\n";
	exemplo2.impressao();
	cout << "\n\n";

	system("pause");

    return 0;
}

//uma funcao menbro so pode chamar objeto const se ela mesmo for const

/* possiveis chamadas nesse caso:
exemplo.getHora();
exemplo.impressao();
exemplo.getMinuto();

exemplo2.setHora();
exemplo2.getHora();
exemplo2.impressao();
exemplo2.getMinuto();
*/
