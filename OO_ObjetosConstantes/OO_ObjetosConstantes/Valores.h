#pragma once

class Tempo { //criamos uma classe

public:  //definimos como publico os metodos
	Tempo(int = 0, int = 0); //determinamos hora e minuto por padra sera zero

	//Funcao de inicio, metodos sem retorno
	void setTempo(int, int);
	void setHora(int);
	void setMinuto(int);

	//Funcoes para pegar valores
	int getHora() const;
	int getMinuto() const;

	//Funcao de impressao
	void impressao() const;

private : //definimos atributos
	int hora;
	int minuto;
};