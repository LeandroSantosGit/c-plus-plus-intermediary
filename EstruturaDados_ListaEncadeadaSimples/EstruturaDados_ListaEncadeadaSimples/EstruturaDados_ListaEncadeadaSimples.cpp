// EstruturaDados_ListaEncadeadaSimples.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include <cstddef>

using namespace std;

class No {

	friend class Lista; //definir classe frienda da classe Lista podendo acessa atributos privados

private:  //atributos
	int valor;
	No *Pproximo;

public: //metodos 
	No(int val) : valor(val), Pproximo(NULL) { //metodo membro construtor com argumentos
		// argumentos iram adcionar valor na lista, o ponteiro Pproximo e null(aponta para o final da lista)
	}

	int PegaValor(void) { //metodo ira retornar valor adcionado na lista
		return valor;
	}

	No* PegaProximo(void) { //metodos ira retornar ponteiro Pproximo(final da lista)
		return Pproximo;
	}
};

class Lista {

private:
	No *PCabeca; 
	No *PCalda;

public: //metodos
	Lista(void); //criar lista vazia
	Lista(int val); //criar lista com valor
	void Imprimir(); //imprimir lista
};

//Implementacao dos medodos
Lista::Lista() { 
	PCabeca = PCalda = NULL; //definindo PCabeca e PCalda como NULL, assim sendo vazias
}

Lista::Lista(int val) { 
	PCabeca = new No(val); //PCabeca é igual ao no do construtor
	PCalda = PCabeca; // PCalda é igual PCabeca, sendo igual ao no do construtor
}

void Lista::Imprimir() { 
	No *p = PCabeca; //criamos ponteiro igual PCabeca

	if (PCabeca == NULL) { //verificar se lista esta vazia
		cout << "Lista vazia" << endl; 
	}

	cout << "Lista: "; 
	while (p != NULL) { //enquanto lista for diferente de nulo
		cout << p->valor; //imprimir valores da lista
		p = p->Pproximo; //ponteiro ira apontar para o proximo valor ate chegar ao valor nulo
	}

	cout << endl;
}

int main()
{
	Lista lista1; //criamos objeto lista vazia
	cout << "Lista1 sem no" << endl;
	cout << "Lista1 " << endl;
	lista1.Imprimir(); 

	Lista lista2(5); //criamos lista com valor
	cout << "\nLista2 com um no" << endl;
	cout << "Lista2 " << endl;
	lista2.Imprimir();

	Lista lista3(2); //criamos lista com valor
	cout << "\nLista3 com um no" << endl;
	cout << "Lista3 " << endl;
	lista3.Imprimir();	

	system("pause");

    return 0;
}

