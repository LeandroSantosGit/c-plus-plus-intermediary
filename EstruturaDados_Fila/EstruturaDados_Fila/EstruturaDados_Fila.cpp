// EstruturaDados_Fila.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>
#include <cstddef>

//primeiro que entra na fila é o primeiro que sai

using namespace std;

class No {
	friend class Lista; //classe amiga Lista

private: //atributos
	int valor;
	No *Pproximo;

public:
	No(int val) :valor(val), Pproximo(NULL) { //metodo construtor
	
	}
};

class Lista {

private: //atributos
	No * PInicio; //ponteiro inicio da lista
	No *PFim; //ponteiro fim da fila
	No *PegaNo(int v); //auxiliar a inserir no 

public: //metodos
	Lista(void); //construtor lista
	
	void AdicionaNoFim(int val); //adiciona um no na frente
	bool RemoveNoFrente(); //remove no
	void Imprimir(); //imprimir lista
};

Lista::Lista() :PInicio(NULL), PFim(NULL) { //iniciar lista vazia, inicio e fim nulo

}

void Lista::AdicionaNoFim(int val) {

	No *AuxiliaP = PegaNo(val); //passar no para auxiliar

	if (PInicio == NULL) { //se for inicio for nulo
		PInicio = PFim = AuxiliaP; //passar valor nulo para inicio e fim
	}
	else { //se nao
		PFim->Pproximo = AuxiliaP; //adicionar valor no fim
		PFim = AuxiliaP;
	}
}

bool Lista::RemoveNoFrente() {

	if (PInicio == NULL) { //se lista estiver vazia 
		return false; //retorna falso, nao pode remover fila vazia
	}
	else { //se nao
		No *Ptemp = PInicio; //criamos ponteiro temporario atrinuimos inicio da fila

		if (PInicio == PFim) { //verificar se inicio da fila e igual ao fim da fila
			PInicio = PFim = NULL; //inicio e fim igual a nulo
		}
		else {
			PInicio = PInicio->Pproximo; //apontar para proximo no que agora sera o primeiro 
		}

		delete Ptemp; //deletando no
		return true;
	}
}

No *Lista::PegaNo(int valor) { 

	No *auxi = new No(valor); //criamos novo no
	return auxi; //retorna novo no
}

void Lista::Imprimir() { //imprimir

	No *p = PInicio; //criamos ponteiro para auxiliar e atribimos inicio da fila

	if (PInicio == NULL) {// verificar se fila esta vazia
		cout << "Fila vazia" << endl;
	}

	cout << "Fila: ";
	while (p != NULL) {//enquanto p for diferente nulo 
		cout << p->valor; //imprimir valores
		p = p->Pproximo; //apontar para poximo valor ate imprimir todos
	}
	cout << endl;
}

class Filas :private Lista { //herda a classe Lista

public: //metodos da classe herdada
	void entra(int val) { //adicionar novos no fim da fila
		AdicionaNoFim(val); 
	}

	bool sai() { //remover da fila
		return RemoveNoFrente(); 
	}

	void imprimeF() { //imprimir fila
		Imprimir();
	}
};

int main()
{
	Filas fila; //criamos fila

	fila.entra(1); //adicionamos elementos na fila
	fila.imprimeF(); //imprimir fila
	fila.entra(2);
	fila.imprimeF();
	fila.entra(3);
	fila.imprimeF();
	fila.sai(); //remover da fila
	fila.entra(5);
	fila.imprimeF(); 
	fila.entra(6);
	fila.imprimeF();
	fila.entra(7);
	fila.imprimeF();
	fila.sai();
	fila.imprimeF(); 

	system("pause");

    return 0;
}

