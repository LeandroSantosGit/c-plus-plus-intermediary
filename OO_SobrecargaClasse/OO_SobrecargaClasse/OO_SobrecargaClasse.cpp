// OO_SobrecargaClasse.cpp : define o ponto de entrada para o aplicativo do console.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

#pragma warning(disable : 4996) //para evitar erros no uso do strncpy

class Numeros { //classe
	char valor[2];

public: //metodos
	Numeros();
	Numeros(const char *i);
	//metodos friend
	friend long operator+ (Numeros x, Numeros y);
	friend long operator- (Numeros x, Numeros y);
	friend long operator* (Numeros x, Numeros y);
	friend long operator/ (Numeros x, Numeros y);
};

//implementando metodos construtor 
Numeros::Numeros() {

	strncpy(valor,"1", 2);
}

Numeros::Numeros(const char *i) {

	strncpy(valor, i, 2);
}

int main()
{
	Numeros x = "10";
	Numeros y = "2";

	cout << "X + Y = " << (x + y) << endl;
	cout << "X - Y = " << (x - y) << endl;
	cout << "X * Y = " << (x * y) << endl;
	cout << "X / y = " << (x / y) << endl;

	system("pause");

    return 0;
}

//implementacao dos metodos
long operator+(Numeros x, Numeros y) {

	return(atol(x.valor) + atol(y.valor));
}

long operator-(Numeros x, Numeros y) {

	return(atol(x.valor) - atol(y.valor));
}

long operator*(Numeros x, Numeros y) {

	return(atol(x.valor) * atol(y.valor));
}

long operator/(Numeros x, Numeros y) {

	return(atol(x.valor) / atol(y.valor));
}

