#include <Windows.h>


//funcao main
int WINAPI WinMain(HINSTANCE hIntance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	MessageBox(NULL, "Ola Mundo!", "TreinaWeb", MB_ICONEXCLAMATION | MB_OK); //caixa de mensagem

	return 0;
}

/*
WinMain alterar a ordem da leitura de dados para ser da esquerda para direita

HINSTANCE hInstance parametros da funcao

HINSTANCE hPrevInstance instacia para indentificar objeto

LPSTR lpCmdLine handle da ultima instancia

int nCmdShow ponteiro para comando da aplicacao
*/
