// Vertex Shader
float4 VS(float4 Pos : POSITION) : SV_POSITION
{
	return Pos;  //No calculations done here, just returns the position of the vertices
}

// Pixel Shader
float4 PS(float4 Pos : SV_POSITION) : SV_Target
{
	return float4(0.0f, 1.0f, 0.0f, 1.0f);    // All pixels put through the pixel shader here will be green.
}

technique10 Tech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}