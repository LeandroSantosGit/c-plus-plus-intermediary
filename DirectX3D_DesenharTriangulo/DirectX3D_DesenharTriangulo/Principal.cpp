#pragma comment(lib, "D3D10.lib")
#pragma comment(lib, "D3Dx10d.lib")

#include <windows.h>
#include <tchar.h>
#include <d3dx10.h>
#include <d3d10.h>
	
LPCTSTR className = TEXT("DirectX-Triangulo");
HWND hwndHandle = NULL;

int width = 800;
int height = 600;

bool InitWin(HINSTANCE hInstance, int width, int heigth);


ID3D10Device* d3dDevice;
IDXGISwapChain* SwapChain;
ID3D10RenderTargetView* RenderTargetView;

//interfaces
ID3D10Effect* FX;
ID3D10InputLayout* VertexLayout;
ID3D10Buffer* VertexBuffer;
ID3D10EffectTechnique* Technique;

struct Vertex {

	D3DXVECTOR3 position;
};

bool ReleaseObj(); 
bool InitializeDirect3dApp(HINSTANCE hInstance);
bool InitScene();
void DrawScene();

LRESULT CALLBACK CBack(HWND, UINT, WPARAM, LPARAM);

int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow) {

	if (!InitWin(hInstance, width, height)) {
		return false;
	}

	if (!InitializeDirect3dApp(hInstance)) {
		return false;
	}

	if (!InitScene()) {
		return false;
	}

	MSG msg = { 0 };
	while (WM_QUIT != msg.message) {

		DrawScene();

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) == TRUE) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

bool InitWin(HINSTANCE hInstance, int width, int heigth) {

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)CBack;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = 0;
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = className;
	wcex.hIconSm = 0;
	RegisterClassEx(&wcex);

	hwndHandle = CreateWindow(className, TEXT("DirectX-Triangulo"), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, NULL, NULL, hInstance, NULL);

	if (!hwndHandle) {
		return false;
	}

	ShowWindow(hwndHandle, SW_SHOW);
	UpdateWindow(hwndHandle);
	return false;
}

LRESULT CALLBACK CBack(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message){
		case WM_KEYDOWN:
			switch (wParam){
				case VK_ESCAPE:
					PostQuitMessage(0);
					break;
			}
		break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

bool InitializeDirect3dApp(HINSTANCE hInstance) {

	DXGI_SWAP_CHAIN_DESC d3d;

	d3d.BufferDesc.Width = width;
	d3d.BufferDesc.Height = height;
	d3d.BufferDesc.RefreshRate.Numerator = 60;
	d3d.BufferDesc.RefreshRate.Denominator = 1;
	d3d.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	d3d.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	d3d.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	d3d.SampleDesc.Count = 1;
	d3d.SampleDesc.Quality = 0;

	d3d.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	d3d.BufferCount = 1;
	d3d.OutputWindow = hwndHandle;
	d3d.Windowed = true;
	d3d.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	d3d.Flags = 0;

	D3D10CreateDeviceAndSwapChain(0, D3D10_DRIVER_TYPE_HARDWARE, 0, 0, D3D10_SDK_VERSION, &d3d, &SwapChain, &d3dDevice);

	ID3D10Texture2D* backBuffer;
	SwapChain->GetBuffer(0, _uuidof(ID3D10Texture2D), reinterpret_cast<void**>(&backBuffer));
	d3dDevice->CreateRenderTargetView(backBuffer, 0, &RenderTargetView);
	backBuffer->Release();

	d3dDevice->OMSetRenderTargets(1, &RenderTargetView, NULL);

	D3D10_VIEWPORT vp;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	vp.Width = width;
	vp.Height = height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	d3dDevice->RSSetViewports(1, &vp);

	D3DXCOLOR bgColor(0.0f, 0.0f, 0.0f, 0.0f);
	d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor);

	SwapChain->Present(0, 0);

	return true;
}

bool InitScene() { //configuracoes do triangulo

	Vertex vertices[] = { 

		D3DXVECTOR3(-0.5f, 0.5f, 0.5f),
		D3DXVECTOR3(0.5f, -0.5f, 0.5f),
		D3DXVECTOR3(-0.5f, -0.5f, 0.5f),
	};

	//preencher buffer de vertices
	D3D10_BUFFER_DESC bd;
	bd.Usage = D3D10_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(Vertex) * 3;
	bd.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	D3D10_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = vertices;
	d3dDevice->CreateBuffer(&bd, &InitData, &VertexBuffer); //criar buffer

	UINT stride = sizeof(Vertex);
	UINT offset = 0;
	d3dDevice->IAGetVertexBuffers(0, 1, &VertexBuffer, &stride, &offset);

	D3D10_INPUT_ELEMENT_DESC layout[] = { {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0}, };

	D3DX10CreateEffectFromFile("Shader.fx", NULL, NULL, "fx_4_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, d3dDevice, NULL, NULL, &FX, NULL, NULL);

	Technique = FX->GetTechniqueByName("Tech");

	//preemche buffer
	D3D10_PASS_DESC PassDesc;
	Technique->GetPassByIndex(0)->GetDesc(&PassDesc);

	d3dDevice->IASetInputLayout(VertexLayout);

	d3dDevice->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return true;
}

bool ReleaseObj() { //liberar objetos criados

	if (d3dDevice) d3dDevice->ClearState();

	if (VertexBuffer) VertexBuffer->Release();
	if (VertexLayout) VertexLayout->Release();
	if (FX) FX->Release();
	if (RenderTargetView) RenderTargetView->Release();
	if (SwapChain) SwapChain->Release();
	if (d3dDevice) d3dDevice->Release();

	return true;
}

void DrawScene() {

	D3DXCOLOR bgColor(0.0f, 0.0f, 0.0f, 1.0f);
	d3dDevice->ClearRenderTargetView(RenderTargetView, bgColor);

	D3D10_TECHNIQUE_DESC techDesc;
	Technique->GetDesc(&techDesc);
	for (UINT p = 0; p < techDesc.Passes; ++p) {
		Technique->GetPassByIndex(p)->Apply(0);
		d3dDevice->Draw(3, 0);
	}

	SwapChain->Present(0, 0);
}